#include "main.h"
#include "epd.h"
#include "../GUI/GUI_Paint.h"
#include "../Data/ImageData.h"
#include "../Data/ImageData2.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_spi.h"

extern SPI_HandleTypeDef hspi1;
void WriteByte(UBYTE value)
{
    HAL_SPI_Transmit(&hspi1, &value, 1, 1000);
}

unsigned char WF_Full_1IN54[159] =
{ 0x80, 0x48, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x48,
		0x80, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x48, 0x40,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x48, 0x80, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0xA, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x8, 0x1,
		0x0, 0x8, 0x1, 0x0, 0x2, 0xA, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 0x0, 0x0,
		0x0, 0x22, 0x17, 0x41, 0x0, 0x32, 0x20 };

// waveform partial refresh(fast)
unsigned char WF_PARTIAL_1IN54_0[159] =
{ 0x0, 0x40, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x80, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x40, 0x40, 0x0, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x80, 0x0, 0x0, 0x0, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		0x0, 0x0, 0x0, 0xF, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x1, 0x1, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
		0x0, 0x0, 0x0, 0x22, 0x22, 0x22, 0x22, 0x22, 0x22, 0x0, 0x0, 0x0, 0x02,
		0x17, 0x41, 0xB0, 0x32, 0x28, };


/* MODULE BEGIN */

int ModuleInit(void)
{
	WritePin(GPIOA, DC_Pin, 0);
	WritePin(GPIOA, CS_PIN, 0);
	WritePin(GPIOA, RST_Pin, 1);
	return 0;
}

void ModuleExit(void)
{
	WritePin(GPIOA, DC_Pin, 0);
	WritePin(GPIOA, CS_PIN, 0);
	WritePin(GPIOA, RST_Pin, 0);
}

/* MODULE END */

static void Reset(void)
{
	WritePin(GPIOA, RST_Pin, 1);
    Delay(200);
    WritePin(GPIOA, RST_Pin, 0);
    Delay(2);
    WritePin(GPIOA, RST_Pin, 1);
    Delay(200);
}

static void ReadBusy(void)
{
    while(ReadPin(GPIOA, BUSY_PIN) == 1) {      //LOW: idle, HIGH: busy
    	Delay(10);
    }
}

void SendCommand(UBYTE cmd)
{
	WritePin(GPIOA, DC_PIN, 0);
	WritePin(GPIOA, CS_PIN, 0);
	WriteByte(cmd);
    WritePin(GPIOA, CS_PIN, 1);
}

void SendData(UBYTE data)
{
	WritePin(GPIOA, DC_PIN, 1);
	WritePin(GPIOA, CS_PIN, 0);
	WriteByte(data);
    WritePin(GPIOA, CS_PIN, 1);
}


static void SetWindows(UWORD Xstart, UWORD Ystart, UWORD Xend, UWORD Yend)
{
	SendCommand(0x44); // SET_RAM_X_ADDRESS_START_END_POSITION
	SendData((Xstart>>3) & 0xFF);
	SendData((Xend>>3) & 0xFF);

	SendCommand(0x45); // SET_RAM_Y_ADDRESS_START_END_POSITION
	SendData(Ystart & 0xFF);
	SendData((Ystart >> 8) & 0xFF);
	SendData(Yend & 0xFF);
	SendData((Yend >> 8) & 0xFF);
}

static void SetCursor(UWORD Xstart, UWORD Ystart)
{
    SendCommand(0x4E); // SET_RAM_X_ADDRESS_COUNTER
    SendData(Xstart & 0xFF);

    SendCommand(0x4F); // SET_RAM_Y_ADDRESS_COUNTER
    SendData(Ystart & 0xFF);
    SendData((Ystart >> 8) & 0xFF);
}

static void Lut(UBYTE *lut)
{
	SendCommand(0x32);
	for(UBYTE i=0; i<153; i++) {
		SendData(lut[i]);
	}
	ReadBusy();
}

static void SetLut(UBYTE *lut)
{
	Lut(lut);

    SendCommand(0x3f);
    SendData(lut[153]);

    SendCommand(0x03);
    SendData(lut[154]);

    SendCommand(0x04);
    SendData(lut[155]);
	SendData(lut[156]);
	SendData(lut[157]);

	SendCommand(0x2c);
    SendData(lut[158]);
}

void Init(void)
{
	Reset();
	ReadBusy();

    SendCommand(0x12);  //SWRESET
    ReadBusy();

    SendCommand(0x01); //Driver output control
    SendData(0xC7);
    SendData(0x00);
    SendData(0x01);

    SendCommand(0x11); //data entry mode
    SendData(0x01);

    SetWindows(0, HEIGHT-1, WIDTH-1, 0);

    SendCommand(0x3C); //BorderWavefrom
    SendData(0x01);

    SendCommand(0x18);
    SendData(0x80);

    SendCommand(0x22); // //Load Temperature and waveform setting.
    SendData(0XB1);
    SendCommand(0x20);

    SetCursor(0, HEIGHT-1);
    ReadBusy();

    SetLut(WF_Full_1IN54);
}

static void TurnOnDisplay(void)
{
    SendCommand(0x22);
    SendData(0xc7);
    SendCommand(0x20);
    ReadBusy();
}


void Clear(void)
{
    UWORD Width, Height;
    Width = (WIDTH % 8 == 0)? (WIDTH / 8 ): (WIDTH / 8 + 1);
    Height = HEIGHT;

    SendCommand(0x24);
    for (UWORD j = 0; j < Height; j++) {
        for (UWORD i = 0; i < Width; i++) {
            SendData(0XFF);
        }
    }
    SendCommand(0x26);
    for (UWORD j = 0; j < Height; j++) {
        for (UWORD i = 0; i < Width; i++) {
            SendData(0XFF);
        }
    }
    TurnOnDisplay();
}

void Sleep(void)
{
    SendCommand(0x10); //enter deep sleep
    SendData(0x01);
    Delay(100);
}

void Display(UBYTE *Image)
{
    UWORD Width, Height;
    Width = (WIDTH % 8 == 0)? (WIDTH / 8 ): (WIDTH / 8 + 1);
    Height = HEIGHT;

    UDOUBLE Addr = 0;
    SendCommand(0x24);
    for (UWORD j = 0; j < Height; j++) {
        for (UWORD i = 0; i < Width; i++) {
            Addr = i + j * Width;
            SendData(Image[Addr]);
        }
    }
    TurnOnDisplay();
}



int Init_EPD(void)
{
	ModuleInit();

	Init();
	Clear();
	Delay(500);

	// draw code here
    //Create a new image cache
    UBYTE *BlackImage;
    /* you have to edit the startup_stm32fxxx.s file and set a big enough heap size */
    UWORD Imagesize = ((WIDTH % 8 == 0)? (WIDTH / 8 ): (WIDTH / 8 + 1)) * HEIGHT;
    if((BlackImage = (UBYTE *)malloc(Imagesize)) == NULL) {
        return -1;
    }

    Paint_NewImage(BlackImage, WIDTH, HEIGHT, 270, WHITE);

//  Paint_SelectImage(BlackImage);
//	Paint_Clear(WHITE);
//	Paint_DrawBitMap(gImage_1in54);
//	Display(BlackImage);
//	Delay(5000);
//  Paint_Clear(WHITE);


    Paint_SelectImage(BlackImage);
	Paint_Clear(WHITE);
	Paint_DrawBitMap(photo_1);
	Display(BlackImage);
	Delay(5000);
	Paint_SelectImage(BlackImage);
	Paint_Clear(WHITE);

	// 2.Drawing on the image
//	Paint_DrawPoint(5, 10, BLACK, DOT_PIXEL_1X1, DOT_STYLE_DFT);
//	Paint_DrawPoint(5, 25, BLACK, DOT_PIXEL_2X2, DOT_STYLE_DFT);
//	Paint_DrawPoint(5, 40, BLACK, DOT_PIXEL_3X3, DOT_STYLE_DFT);
//	Paint_DrawPoint(5, 55, BLACK, DOT_PIXEL_4X4, DOT_STYLE_DFT);
//
//	Paint_DrawLine(20, 10, 70, 60, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);
//	Paint_DrawLine(70, 10, 20, 60, BLACK, DOT_PIXEL_1X1, LINE_STYLE_SOLID);
//	Paint_DrawLine(170, 15, 170, 55, BLACK, DOT_PIXEL_1X1, LINE_STYLE_DOTTED);
//	Paint_DrawLine(150, 35, 190, 35, BLACK, DOT_PIXEL_1X1, LINE_STYLE_DOTTED);
//
//	Paint_DrawRectangle(20, 10, 70, 60, BLACK, DOT_PIXEL_1X1, DRAW_FILL_EMPTY);
//	Paint_DrawRectangle(85, 10, 130, 60, BLACK, DOT_PIXEL_1X1, DRAW_FILL_FULL);
//
//	Paint_DrawCircle(170, 35, 20, BLACK, DOT_PIXEL_1X1, DRAW_FILL_EMPTY);
//	Paint_DrawCircle(170, 85, 20, BLACK, DOT_PIXEL_1X1, DRAW_FILL_FULL);
//	Paint_DrawString_EN(5, 85, "waveshare", &Font20, BLACK, WHITE);
//	Paint_DrawNum(5, 110, 123456789, &Font20, BLACK, WHITE);
//
//	Paint_DrawString_CN(5, 135,"���abc", &Font12CN, BLACK, WHITE);
//	Paint_DrawString_CN(5, 155, "΢ѩ����", &Font24CN, WHITE, BLACK);
//
//	Display(BlackImage);
//	Delay(2000);



	Init();
	Clear();
	Sleep();

	ModuleExit();

	return 0;
}
