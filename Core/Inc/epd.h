/*
 * epd.h
 *
 *  Created on: Sep 25, 2022
 *      Author: hanuman
 */

#ifndef INC_EPD_H_
#define INC_EPD_H_

#include <stdlib.h> // malloc() free()
#include "main.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include <stdint.h>
#include <stdio.h>

#define UBYTE   	uint8_t
#define UWORD   	uint16_t
#define UDOUBLE 	uint32_t
#define WIDTH       200
#define HEIGHT      200
#define Delay(d)	HAL_Delay(d)

#define WritePin(_gpiox, _pin, _value) HAL_GPIO_WritePin(_gpiox, _pin, _value == 0? GPIO_PIN_RESET:GPIO_PIN_SET)
#define ReadPin(_gpiox, _pin) HAL_GPIO_ReadPin(_gpiox, _pin)

int Init_EPD(void);
void WriteByte(UBYTE value);

#endif /* INC_EPD_H_ */
